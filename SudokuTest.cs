using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuTest
{
    class SudokuPuzzleValidator
    {
        static void Main(string[] args)
        {
            int[][] goodSudoku1 = {
                new int[] {7,8,4,  1,5,9,  3,2,6},
                new int[] {5,3,9,  6,7,2,  8,4,1},
                new int[] {6,1,2,  4,3,8,  7,5,9},

                new int[] {9,2,8,  7,1,5,  4,6,3},
                new int[] {3,5,7,  8,4,6,  1,9,2},
                new int[] {4,6,1,  9,2,3,  5,8,7},

                new int[] {8,7,6,  3,9,4,  2,1,5},
                new int[] {2,4,3,  5,6,1,  9,7,8},
                new int[] {1,9,5,  2,8,7,  6,3,4}
            };


            int[][] goodSudoku2 = {
                new int[] {1,4, 2,3},
                new int[] {3,2, 4,1},

                new int[] {4,1, 3,2},
                new int[] {2,3, 1,4}
            };

            int[][] badSudoku1 =  {
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},

                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},

                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9}
            };

            int[][] badSudoku2 = {
                new int[] {1,2,3,4,5},
                new int[] {1,2,3,4},
                new int[] {1,2,3,4},
                new int[] {1}
            };

            Debug.Assert(ValidateSudoku(goodSudoku1), "This is supposed to validate! It's a good sudoku!");
            Debug.Assert(ValidateSudoku(goodSudoku2), "This is supposed to validate! It's a good sudoku!");
            Debug.Assert(!ValidateSudoku(badSudoku1), "This isn't supposed to validate! It's a bad sudoku!");
            Debug.Assert(!ValidateSudoku(badSudoku2), "This isn't supposed to validate! It's a bad sudoku!");
        }

        static bool ValidateSudoku(int[][] puzzle)
        {
            Sudoku sudoku = new Sudoku(puzzle);
            return sudoku.Validate();
        }
    }

    public class Sudoku
    {
        private int[][] sudoku;
        private int squireCount = 0;

        public Sudoku(int[][] sudoku)
        {
            this.sudoku = sudoku;
            this.squireCount = Convert.ToInt32(Math.Round(Math.Sqrt(Convert.ToDouble(this.sudoku.GetLength(0))), 2));
        }

        public bool Validate()
        {
            bool verticalLinesAreValid = this.VerticalLines.All(line => IsValid(line));
            bool horizontalLinesAreValid = this.HorizontalLines.All(line => IsValid(line));
            bool squaresAreValid = this.Squares.All(square => IsValid(square));

            return verticalLinesAreValid && squaresAreValid && horizontalLinesAreValid;

        }

        private IEnumerable<IEnumerable<int>> VerticalLines
        {
            get
            {
                return from line in sudoku select line;
            }
        }

        private IEnumerable<IEnumerable<int>> HorizontalLines
        {
            get
            {
                return
                    from y in Enumerable.Range(0, this.squireCount)
                    select (
                        from x in Enumerable.Range(0, this.squireCount)
                        select this.sudoku[x][y]
                     );
            }
        }

        private IEnumerable<IEnumerable<int>> Squares
        {
            get
            {
                return
                    from x in Enumerable.Range(0, this.squireCount)
                    from y in Enumerable.Range(0, this.squireCount)
                    select this.GetSquare(x, y);
            }
        }

        private IEnumerable<int> GetSquare(int x, int y)
        {
            return
                from squareX in Enumerable.Range(0, this.squireCount)
                from squareY in Enumerable.Range(0, this.squireCount)
                select sudoku[x * this.squireCount + squareX][y * this.squireCount + squareY];
        }

        private static bool IsValid(IEnumerable<int> line)
        {
            return !(
                from item in line
                group item by item into g
                where g.Count() > 1
                select g).Any();
        }

    }
}
